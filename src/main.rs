// #![deny(warnings)]
#![feature(const_fn)]

extern crate orbital_core;

extern crate orbclient;
extern crate orbfont;
extern crate syscall;

use orbital_core::{
    Orbital, Handler, Properties
};
use orbclient::{
    Color, Event
};
use std::{
    env, cmp,
    process::Command,
    collections::BTreeMap
};
use syscall::{
    Packet
};


type WindowID = usize;



fn main() {
    // Daemonize
    if unsafe { syscall::clone(0).unwrap() } == 0 {
        let mut args = env::args().skip(1);

        let display_path = args.next().expect("orbital: no display argument");
        let login_cmd = args.next().expect("orbital: no login manager argument");

        orbital_core::fix_env(&display_path).unwrap();

        let display = Orbital::open_display(&display_path);

        match display {
            Ok(display) => {
                println!("orbital: found display {}x{}", display.width, display.height);
                let scheme = WmHandler::new(
                    display.width as u32,
                    display.height as u32,
                );

                Command::new(&login_cmd)
                    .args(args)
                    .spawn()
                    .expect("orbital: failed to launch login cmd");

                display.run(scheme).expect("orbital: failed to run main loop");
            },
            Err(err) => println!("orbital: could not register orbital: {}", err)
        }
    }
}

struct Window {
    x: i32,
    y: i32,
    w: u32,
    h: u32,
    title: String,
}


struct WmHandler {
    screen_w: u32,
    screen_h: u32,
    next_id: WindowID,
    windows: BTreeMap<WindowID, Window>,
}
impl WmHandler {
    pub fn new(width: u32, height: u32) -> WmHandler {
        WmHandler {
            screen_w: width,
            screen_h: height,
            next_id: 0,
            windows: BTreeMap::new(),
        }
    }
}

impl Handler for WmHandler {
    fn should_delay(&mut self, packet: &Packet) -> bool {
        false // TODO
    }
    fn handle_scheme(&mut self, orb: &mut Orbital, packets: &mut [Packet]) -> std::io::Result<()> {
        unimplemented!()
    }

    fn handle_display(&mut self, orb: &mut Orbital, events: &mut [Event]) -> std::io::Result<()> {
        unimplemented!()
    }

    fn handle_window_new(
        &mut self, orb: &mut Orbital, x: i32, y: i32, width: i32, 
        height: i32, flags: &str, title: String) -> syscall::Result<usize> {

        let x = cmp::max(0, cmp::min(self.screen_w as i32, x));
        let y = cmp::max(0, cmp::min(self.screen_h as i32, y));
        let id = self.next_id;
        self.next_id += 1;
        self.windows.insert(id, Window {
            x: x,
            y: y,
            w: width as u32,
            h: height as u32,
            title: title,
        });
        Ok(id)
    }

    fn handle_window_read(
        &mut self, orb: &mut Orbital, id: usize, buf: &mut [Event]) -> syscall::Result<usize> {
        unimplemented!()
    }

    fn handle_window_position(
        &mut self, orb: &mut Orbital, id: usize, x: Option<i32>, y: Option<i32>) -> syscall::Result<()> {
        unimplemented!()
    }

    fn handle_window_resize(
        &mut self, 
        orb: &mut Orbital, 
        id: usize, 
        w: Option<i32>, 
        h: Option<i32>
    ) -> syscall::Result<()> {
        unimplemented!()
    }

    fn handle_window_title(
        &mut self, 
        orb: &mut Orbital, 
        id: usize, 
        title: String
    ) -> syscall::Result<()> {
        unimplemented!()
    }

    fn handle_window_clear_notified(
        &mut self, 
        orb: &mut Orbital, 
        id: usize
    ) -> syscall::Result<()> {
        unimplemented!()
    }

    fn handle_window_map(
        &mut self, 
        orb: &mut Orbital, 
        id: usize
    ) -> syscall::Result<&mut [Color]> {
        unimplemented!()
    }

    fn handle_window_properties(
        &mut self, 
        orb: &mut Orbital, 
        id: usize
    ) -> syscall::Result<Properties> {
        unimplemented!()
    }

    fn handle_window_sync(
        &mut self, 
        orb: &mut Orbital, 
        id: usize
    ) -> syscall::Result<usize> {
        unimplemented!()
    }

    fn handle_window_close(
        &mut self, 
        orb: &mut Orbital, 
        id: usize
    ) -> syscall::Result<usize> {
        unimplemented!()
    }
}
